import React, { useState, useEffect } from "react";
import { IViewProfile } from "./profileTypes";
import Modal from "../../components/common/Modal";
import { compressImages } from "../../assets/js/imageCompress";
import { useDispatch } from "react-redux";
import { updateProfilePic } from "../../store/Actions/userprofileActions";

interface IProps {
  viewProfile: IViewProfile;
}

interface IProfileItemProps {
  pKey: string;
  pValue: string | number;
}

const ProfileItem = ({ pKey, pValue }: IProfileItemProps) => (
  <div className="card-category profile-view-item">
    <span style={{ fontWeight: "bold" }}>{pKey}: </span>
    <span>{pValue}</span>
  </div>
);

const ViewProfile = ({ viewProfile }: IProps) => {
  const [disableSave, setDisableSave] = useState(true);
  const [pic, setPic] = useState("");
  const [selectedImage, setSelectedImage] = useState("");

  const dispatch = useDispatch();

  const imageSelectedHandler = async (event: any) => {
    let image = event.target.files[0];
    compressImages([image]).map((x: any) => {
      return x.then((res: any) => {
        setSelectedImage(res);
        setDisableSave(false);
      });
    });
  };
  const handlePicSave = () => {
    setDisableSave(true);
    updateProfilePic(dispatch, selectedImage);
  };

  useEffect(() => {
    let pic = viewProfile.picture;
    if (!pic) pic = require(`../../assets/img/account_circle.png`);
    setPic(pic);
  }, [viewProfile.picture]);

  return (
    <div className="flex flex-column">
      <div className="card card-profile">
        <div className="card-avatar">
          <img
            style={{ borderRadius: "50%", width: "130px", height: "130px" }}
            src={viewProfile.picture}
            alt="profile pic"
          />
          <i
            className="material-icons profile-camera-icon"
            data-toggle="modal"
            data-target="#profile-pic"
          >
            camera_alt
          </i>
        </div>
        <Modal
          modalId="profile-pic"
          modalTitle="Profile Pic"
          saveAction={handlePicSave}
          disableSave={disableSave}
        >
          <div
            style={{ display: "flex", width: "fit-content", margin: "auto" }}
          >
            <img
              alt="profile-full-size"
              src={!!selectedImage ? URL.createObjectURL(selectedImage) : pic}
              style={{ borderRadius: "50%", width: "175px", height: "175px" }}
            />
            <div
              className="profile-pic-edit-view"
              style={{ height: "fit-content" }}
            >
              <label htmlFor="image-input">
                <div style={{ cursor: "pointer" }}>
                  <i className="material-icons">edit</i>
                </div>
                <input
                  id="image-input"
                  type="file"
                  style={{ display: "none" }}
                  onChange={imageSelectedHandler}
                  accept="image/*"
                />
              </label>
            </div>
          </div>
        </Modal>
        <div className="card-body">
          <h6 className="card-category" style={{ textTransform: "none" }}>
            {viewProfile.email}
          </h6>
          <h4 className="card-title" style={{ marginBottom: "2vh" }}>
            {viewProfile.name}
          </h4>
          <ProfileItem pKey="Excel ID" pValue={viewProfile.id} />
          <div className="card-category profile-view-item">
            <span style={{ fontWeight: "bold", alignSelf: "center" }}>
              QR Code:{" "}
            </span>
            <img
              src={viewProfile.qrCodeUrl}
              alt="QR Code"
              style={{ width: "100px" }}
            />
          </div>
          <ProfileItem
            pKey="Institution Name"
            pValue={viewProfile.institutionName}
          />
          <ProfileItem pKey="Gender" pValue={viewProfile.gender} />
          <ProfileItem pKey="Mobile Number" pValue={viewProfile.mobileNumber} />
          <ProfileItem
            pKey="Category"
            pValue={
              !!viewProfile.category
                ? viewProfile.category.charAt(0).toUpperCase() +
                viewProfile.category.slice(1)
                : ""
            }
          />
          <ProfileItem pKey="Referral Code" pValue={viewProfile.referrerAmbassadorId || ''} />
        </div>
      </div>
    </div>
  );
};

export default ViewProfile;
