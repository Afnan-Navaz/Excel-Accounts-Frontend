export interface IUpdateProfile {
  name: string;
  institutionId: number | null;
  institutionName: string | null;
  gender: string;
  mobileNumber: string;
  category: string;
}

export interface IViewProfile {
  id: number;
  name: string;
  email: string;
  picture: string;
  qrCodeUrl: string;
  institutionName: string;
  gender: string;
  mobileNumber: string;
  category: string;
  referrerAmbassadorId: number | null;
}

export interface I_InstitutionListItem {
  id: number;
  name: string;
}
