import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.scss";

import Base from "./pages/Base";
import NotFound from "./pages/NotFound/NotFound";

import { Provider } from "react-redux";
import store from "./store/store";

store.subscribe(() => { });

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/" component={Base} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
