import React from "react";
import "./DottedLineLoader.scss";

const DottedLineLoader = ({ className }: { className?: string }) => {
  return (
    <div className={`dotted-line-spinner ${className ? className : ''}`}>
      <div className="bounce1" />
      <div className="bounce2" />
      <div className="bounce3" />
    </div>
  );
};

export default DottedLineLoader;
