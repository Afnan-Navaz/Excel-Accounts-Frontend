// import React from 'react';

const isLoggedIn = () => (localStorage.getItem("jwt_token") ? true : false);

interface IProps {
  children: any;
}

const PrivateRoute = ({ children }: IProps) => {
  if (isLoggedIn()) {
    return children;
  }
  const { origin } = window.location;
  if (origin !== "http://localhost:3000")
    window.location.href = `${origin}/auth/login?redirect_to=${window.location.href}`;
  else
    window.location.href = `http://localhost:1000/auth/login?redirect_to=${window.location.href}`;
  return null;
};

export default PrivateRoute;
