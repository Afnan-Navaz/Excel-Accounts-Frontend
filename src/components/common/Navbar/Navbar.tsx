import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

import "./Navbar.scss";
import { routes } from "../Sidebar/routes";
import { selectUsername, selectUserProfilePic } from "../../../store/Selectors/userprofileSelectors";

const Navbar = () => {
  const route = routes.find((x) => window.location.pathname === x.path);
  const { name, picture } = useSelector((state: any) => ({
    name: selectUsername(state),
    picture: selectUserProfilePic(state),
  }));

  return (
    <nav className="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
      <div className="container-fluid">
        <div className="navbar-wrapper">
          <Link className="navbar-brand" to={route?.path || ""}>
            {route?.name}
          </Link>
        </div>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          aria-controls="navigation-index"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="sr-only">Toggle navigation</span>
          <span className="navbar-toggler-icon icon-bar"></span>
          <span className="navbar-toggler-icon icon-bar"></span>
          <span className="navbar-toggler-icon icon-bar"></span>
        </button>
        <div className="collapse navbar-collapse justify-content-end">
          <ul className="navbar-nav">
            <li className="nav-item dropdown">
              <a
                className="nav-link navbar-profile"
                href="#"
                id="navbarDropdownMenuLink"
                data-toggle="dropdown"
              >
                <p className="d-inline">{name}</p>
                <img
                  src={
                    !!picture
                      ? picture
                      : require("../../../assets/img/account_circle.png")
                  }
                  alt="profile"
                />
              </a>
              <div
                className="dropdown-menu dropdown-menu-right"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <a
                  className="dropdown-item navbar-text"
                  href={`${window.location.origin}/auth/logout?redirect_to=${window.location.origin}`}
                >
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
